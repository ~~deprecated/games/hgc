return {
  screen  = {
    ntsc = {
      width  = 256,
      height = 224,
      fps    = 29.97,
      origin = {
        x =  0,
        y = -8,
      },
    },
    pal  = {
      width  = 256,
      height = 240,
      fps    = 25,
      origin = {
        x =  0,
        y =  0,
      },
    },
  },
  colours = {
    total       = 54,
    limit       = 25,
    background  = true,
    transparent = 'colourKey',
  },
  meshes  = false,
  shaders = {
    api          = false,
    version      = false,
    fragment     = false,
    vectex       = false,
    geometry     = false,
    tessellation = false,
  },
  physic  = false,
  sprites = {
    onScreen  = 64,
    onLine    =  8,
    palettes  = {
      specify = true,
      {
        colours = 3,
        keys    = 1,
      },
      {
        colours = 3,
        keys    = 1,
      },
      {
        colours = 3,
        keys    = 1,
      },
      {
        colours = 3,
        keys    = 1,
      },
    },
    transform = {
      move   = true,
      rotate = false,
      scale  = false,
      shear  = false,
      affine = false,
    },
    sizes     = {
      {
        width  =  8,
        height =  8,
      },
      {
        width  =  8,
        height = 16,
      },
    },
  },
  tiles   = {
    layers = {
      {
        independent = false,
        palettes    = {
          specify = true,
          {
            colours = 3,
            keys    = 1,
          },
          {
            colours = 3,
            keys    = 1,
          },
          {
            colours = 3,
            keys    = 1,
          },
          {
            colours = 3,
            keys    = 1,
          },
        },
        mirror      = true,
        transform   = {
          individual = true,
          move       = true,
          rotate     = false,
          scale      = false,
          shear      = false,
          affine     = false,
        },
        sizes       = {
          {
            width  = 16,
            height = 16,
          },
        },
        map         = false,
      },
    },
  },
}
