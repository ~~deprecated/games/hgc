return {
  screen  = {
    fhd   = {
      width  = 1920,
      height = 1080,
      fps    = 30,
      origin = {
        x = 0,
        y = 0,
      },
    },
    uhd4k = {
      width  = 3840,
      height = 2160,
      fps    = 30,
      origin = {
        x = 0,
        y = 0,
      },
    },
  },
  colours = {
    total       = '32bpp',
    limit       = false,
    background  = true,
    transparent = 'alpha',
  },
  meshes  = true,
  shaders = {
    api          = 'glsl_es',
    version      = '1.0.17',
    fragment     = true,
    vectex       = true,
    geometry     = false,
    tessellation = false,
  },
  physic  = true,
  sprites = {
    onScreen  = false,
    onLine    = false,
    palettes  = {
      specify = true,
      free = {
        colours = '24bpp',
        keys    =  '8bpp',
      },
    },
    transform = {
      move   = true,
      rotate = true,
      scale  = true,
      shear  = true,
      affine = true,
    },
    sizes     = {
      {
        width  = 'free',
        height = 'free',
      },
    },
  },
  tiles   = {
    layers = {
      free = {
        independent = true,
        palettes    = {
          specify = true,
          free = {
            colours = '24bpp',
            keys    =  '8bpp',
          },
        },
        mirror      = true,
        transform   = {
          individual = true,
          move       = true,
          rotate     = true,
          scale      = true,
          shear      = true,
          affine     = true,
        },
        sizes       = {
          {
            width  = 'free',
            height = 'free',
          },
        },
        map         = false,
      },
    },
  },
}
