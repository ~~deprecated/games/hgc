return {
  screen  = {
    ntsc = {
      width  = 240,
      height = 192,
      fps    = 29.97,
      origin = {
        x = 0,
        y = 0,
      },
    },
  },
  colours = {
    total       = '15+1',
    limit       = '15+1',
    background  = false,
    transparent = 'transparentColour',
  },
  meshes  = false,
  shaders = {
    api          = false,
    version      = false,
    fragment     = false,
    vectex       = false,
    geometry     = false,
    tessellation = false,
  },
  physic  = false,
  sprites = false,
  tiles   = {
    layers = {
      {
        independent = false,
        palettes    = {
          specify = true,
          free = {
            colours = 2,
            keys    = false,
          },
        },
        mirror      = false,
        transform   = {
          individual = false,
          move       = false,
          rotate     = false,
          scale      = false,
          shear      = false,
          affine     = false,
        },
        sizes       = {
          {
            width  =   6,
            height =   8,
          },
        },
        map         = {
          width  = 240,
          height = 192,
          tiles  = false,
          pixels = false,
        },
      },
    },
  },
}
