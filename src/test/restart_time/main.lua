function love.load()
  -- window --
  love.window.setMode(250, 250)

  -- time --
  upTime = love.timer.getTime()
end

function love.keypressed(key, scancode)
  -- exit --
  if scancode == 'q' or scancode == 'escape' then
    love.event.quit()
  end

  -- restart --
  if scancode == 'r' then
    restartKey = true
    restartTime = love.timer.getTime() - upTime
  end
end

function love.update(dt)
  -- time --
  currentTime = love.timer.getTime() - upTime
  if restartKey == true then
    currentTime = currentTime - restartTime
  end
end

function love.draw()
  -- time --
  love.graphics.print(currentTime)
end
