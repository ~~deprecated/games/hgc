function love.load()
  -- window --
  love.window.setMode(250, 250)

  -- animation --
  delta = 0
  fps   = 1

  a_animation = 1
  a_frames = 7
  a_draw   = 'A'

  b_animation = 1
  b_frames = 3
  b_draw   = 'B'

  animation = a_animation
  frames    = a_frames
  draw      = a_draw
end

function love.keypressed(key, scancode)
  -- exit --
  if scancode == 'q' or scancode == 'escape' then
    love.event.quit()
  end

  -- animation --
  if scancode == 'a' then
    animationKey = true
    animation    = b_animation
    frames       = b_frames
  end
end

function love.keyreleased(key, scancode)
  -- animation --
  if scancode == 'a' then
    animationKey = false
    animation    = a_animation
    frames       = a_frames
  end
end

function love.update(dt)
  -- animation --
  delta = delta + dt
  if delta >= (1 / fps) then
    delta = delta - (1 / fps)
    if animation < frames then
      animation = animation + 1
    else
      animation = 1
    end
  end

  if animationKey == true then
    draw = b_draw
  elseif animationKey == false then
    draw = a_draw
  end
end

function love.draw()
  -- animation --
  love.graphics.print(draw,         0)
  love.graphics.print(animation,   10)
end
