local update = {}

update.time = function(dt)
  nextTime = nextTime + (1 / fps)
end


update.animation = function(metaSprite, images, dt)
  --Increase actual elapsed time with new dt
  metaSprite.elapsedTime = metaSprite.elapsedTime + dt --AnimationStart

  --In order to know if it is necessary to change the frame
  if metaSprite.elapsedTime >= (1 / metaSprite.fps) then
    metaSprite.elapsedTime = metaSprite.elapsedTime - (1 / metaSprite.fps)
    if metaSprite.currentFrame == # metaSprite then --If current frame is equal to long metaSprite list
      metaSprite.currentFrame = 1 -- Return to first frame
    else
      metaSprite.currentFrame = metaSprite.currentFrame + 1 --Next frame
    end
  end

  images.quad = metaSprite[metaSprite.currentFrame] --Update with new fame
end

update.motion = function(character, images, metaSprite, dt)
  -- Check if press button up/down, left/right at same time, no button press and character is not jumping then metaSprite is stand
  if ((character.actionUp == true and character.actionDown == true) or (character.actionLeft == true and character.actionRight == true) or (character.actionLeft == false and character.actionRight == false and character.actionUp == false and character.actionDown == false)) and character.actionA == false and character.jump.isJumping == false then
    if metaSprite ~= character.stand then
        print("(update.motion) -> load stand meta sprite")
        metaSprite = character.stand
        metaSprite.currentFrame = 1
        metaSprite.fps = 9
    end
  end

  -- Check if it is jumping or falling
  if character.jump.higher > 0 and character.actionUp == false and character.actionDown == false and character.actionA == true then
    if metaSprite ~= character.jump then
      print("(update.motion) -> load jump meta sprite")
      metaSprite = character.jump
      metaSprite.currentFrame = 1
      metaSprite.fps = 6
    end

    if character.jump.limitButtonJump == false then
      character.jump.higher   = character.jump.higher - dt
      character.jump.velocity = character.jump.velocity + character.jump.height * (dt / character.jump.higherMax)
    end
  elseif (character.jump.velocity > 0 and character.actionUp == false and character.actionDown == false) then
    if metaSprite ~= character.fall then
      print("(update.motion) -> load fall meta sprite")
      metaSprite = character.fall
      metaSprite.currentFrame = 1
      metaSprite.fps = 6
    end
  end

  --Check if left button has been pressed
  if character.actionLeft == true and character.actionRight == false then
    if character.jump.higher > 0 and character.actionA == false and character.jump.isJumping == false and metaSprite ~= character.walk then
      print("(update.motion) -> load walk meta sprite")
      metaSprite = character.walk
      metaSprite.currentFrame = 1
      metaSprite.fps = 9
    end
    character.position.x = character.position.x - (character.velocity * dt)
    character.scale.x = -1
  end

  --Check if right button has been pressed
  if character.actionRight == true and character.actionLeft == false then
    if character.jump.higher > 0 and character.actionA == false and character.jump.isJumping == false and metaSprite ~= character.walk then
      print("(update.motion) -> load walk meta sprite")
      metaSprite = character.walk
      metaSprite.currentFrame = 1
      metaSprite.fps = 9
    end
    character.position.x = character.position.x + (character.velocity * dt)
    character.scale.x =  1
  end

  --Check if up button has been pressed
  if character.actionUp == true and character.actionDown == false then
    if metaSprite ~= character.walk then
      print("(update.motion) -> load walk meta sprite")
      metaSprite = character.walk
      metaSprite.currentFrame = 1
      metaSprite.fps = 9
    end
    character.position.y = character.position.y - (character.velocity * dt)
    character.jump.ground = character.position.y
  end

  --Check if down button has been pressed
  if character.actionDown == true and character.actionUp == false then
    if metaSprite ~= character.walk then
      print("(update.motion) -> load walk meta sprite")
      metaSprite = character.walk
      metaSprite.currentFrame = 1
      metaSprite.fps = 9
    end
    character.position.y = character.position.y + (character.velocity * dt)
    character.jump.ground = character.position.y
  end

  return metaSprite
end

update.gravity = function(character, dt)
  if character.jump.velocity ~= 0 then
    character.jump.isJumping = true
    character.position.y = character.position.y + (character.jump.velocity * dt)
    character.jump.velocity = character.jump.velocity - (character.gravity * dt)
  end

  if character.position.y > character.jump.ground then
    character.jump.velocity = 0
    character.position.y = character.jump.ground
    character.jump.higher = character.jump.higherMax

    character.jump.limitButtonJump = false
    character.jump.isJumping = false
    character.actionA = false

    if love.keyboard.isScancodeDown(controller.player1.up) then
      character.actionUp = true
    elseif love.keyboard.isScancodeDown(controller.player1.down) then
      character.actionDown = true
    end
  end
end

update.limit = function(character, dt)
  if character.position.x <= character.origin.x then
    character.position.x = character.origin.x
  elseif character.position.x >= windowProfile.mode.width - character.origin.x then
    character.position.x = windowProfile.mode.width - character.origin.x
  end

  if character.position.y <= character.origin.y then
    character.position.y = character.origin.y
  elseif character.position.y >= windowProfile.mode.height - character.origin.y then
    character.position.y = windowProfile.mode.height - character.origin.y
  end
end

return update
