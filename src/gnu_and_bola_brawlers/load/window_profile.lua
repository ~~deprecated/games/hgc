--[[ Contains general parameters about the window ]]--

return {
  mode = {
    width  = 256,
    height = 240,
  },
  scale = {
    x = 2,
    y = 2,
  },
  title = "GNU & Bola Brawlers",
}
