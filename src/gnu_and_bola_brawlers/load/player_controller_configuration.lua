--[[ Contains the players controllers and translate physic button (keyboard, gamepad, etc.) to a NES button name. ]]--

return {
  player1 = {
      left   = 'a',
      right  = 'd',
      up     = 'w',
      down   = 's',
      a      = 'j',
      b      = 'k',
      select = 'g',
      start  = 'h',
      quit   = 'escape',
  }
}
