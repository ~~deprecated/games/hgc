function love.load()
  local load = require 'load/default'
  load.initialize()

  update = require 'update/default'
  draw = require 'draw/default'
end

function love.keypressed(key, scancode)
  local keypressed = require 'keypressed/default'
  metaSprite.bola = keypressed.trigger(character.bola, images.bola, metaSprite.bola, controller.player1, key, scancode)
end

function love.keyreleased(key, scancode)
  local keyreleased = require 'keyreleased/default'
  keyreleased.trigger(character.bola, controller.player1, key, scancode)
end

function love.update(dt)
  update.time(dt)
  update.animation(metaSprite.bola, images.bola, dt)
  metaSprite.bola = update.motion(character.bola, images.bola, metaSprite.bola, dt)
  update.gravity(character.bola, dt)
  update.limit(character.bola, dt)
end

function love.draw()
  draw.refresh()
  draw.object(images.bola, character.bola)
end
