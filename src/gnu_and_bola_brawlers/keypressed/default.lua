local keypressed = {}

keypressed.trigger = function(character, images, metaSprite, controller, key, scancode)
  if scancode == controller.quit then
    love.event.quit()
  end

  if scancode == controller.a  then
    character.actionA = true
    character.actionDown = false
    character.actionUp = false
  end

  if scancode == controller.left then
    character.actionLeft = true
  end

  if scancode == controller.right then
    character.actionRight = true
  end

  if scancode == controller.up and character.jump.isJumping == false then
    character.actionUp = true
  end

  if scancode == controller.down and character.jump.isJumping == false then
    character.actionDown = true
  end

  return metaSprite
end

return keypressed
