local keyreleased = {}

keyreleased.trigger = function(character, controller, key, scancode)
  if scancode == controller.a then
    character.actionA     = false
  end

  if scancode == controller.left then
    character.actionLeft  = false
  end

  if scancode == controller.right then
    character.actionRight = false
  end

  if scancode == controller.up then
    character.actionUp    = false
  end

  if scancode == controller.down then
    character.actionDown  = false
  end

  if scancode == controller.b then
    character.actionB     = false
  end

  if scancode == controller.a then
    character.actionA = false

    if character.jump.velocity ~= 0 then
      character.jump.limitButtonJump = true
    end
  end
end

return keyreleased
